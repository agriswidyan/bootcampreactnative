console.log("Soal 1")
function range(startNum, finishNum){
    var rangeNum = []
    if(startNum <= finishNum){
      for(var i=startNum; i <= finishNum;i++){
        rangeNum.push(i);
      }
    } else if(startNum >= finishNum){
      for(var i=startNum;i >= finishNum;i--){
        rangeNum.push(i);
      }
    }else if (!startNum || !finishNum){
      return -1
    }
    return rangeNum
  }
  
  console.log(range(1,10))
  console.log(range(1))
  console.log(range(11,18))
  console.log(range(54,50))
  console.log(range())

  console.log("---------------------------------------")
  console.log("Soal 2")

  function rangeWithStep(startNum,finishNum,step){
    var rangeNum = []
  
    if(startNum <= finishNum){
      var num = startNum;
      for(var i=0;num <= finishNum;i++){
        rangeNum.push(num)
        num += step
      }
    } else if(startNum >= finishNum){
      var num = startNum;
      for(var i=0; num >= finishNum;i++){
        rangeNum.push(num)
        num -= step
      }
    }else if (!startNum || !finishNum || !step){
      return -1
    }
    return rangeNum
  }
  
  console.log(rangeWithStep(1, 10, 2)) 
  console.log(rangeWithStep(11, 23, 3)) 
  console.log(rangeWithStep(5, 2, 1)) 
  console.log(rangeWithStep(29,2,4))


  console.log("-------------------------------------------")
  console.log("Soal 3")

  function sum(startNum,finishNum,step){
    var rangeNum = []
    var total =0
  
  
    if(!step){
      step=1
    }
  
    if(startNum <= finishNum ){
      var num = startNum;
      
      for(var i=0;num <= finishNum;i++){
        rangeNum.push(num)
        num += step
      }
      for(var i=0;i<rangeNum.length;i++){
        total += rangeNum[i]
      }
  
    } else if(startNum >= finishNum){
      var num = startNum;
      for(var i=0; num >= finishNum;i++){
        rangeNum.push(num)
        num -= step
      }
      for(var i=0;i<rangeNum.length;i++){
        total += rangeNum[i]
      }
    }else if(!finishNum) {
      return startNum
    }
    return total
  }
  
  
  console.log(sum(1,10))
  console.log(sum(5, 50, 2)) 
  console.log(sum(15,10)) 
  console.log(sum(20, 10, 2)) 
  console.log(sum(1)) 
  console.log(sum())  

  console.log("-------------------------------------------")

  
